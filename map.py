import csv
from colorama import Fore, Style
from selenium import webdriver
from selenium.webdriver.common.by import By


class Search:

	def __init__(self):
		self.names , self.address = self.lists('name.csv')
		self.array_time = [[0 for col in self.address] for row in self.address]

	def lists(self, File):
		with open(File, 'r') as f:
			data = csv.reader(f)
			file = list(data)
		names , address = [],[]
		for name , addres in file:
			names.append(name)
			address.append(addres)
		return names , address

	def site(self):
		self.driver = webdriver.Chrome()
		self.driver.get("https://www.google.co.in/maps/@10.8091781,78.2885026,7z")
		self.driver.implicitly_wait(10)

	# search locations
	def searchplace(self, adress):
		Place = self.driver.find_element(By.ID, "searchboxinput")
		Place.send_keys(adress)
		Submit = self.driver.find_element(By.ID,"searchbox-searchbutton")
		Submit.click()

	# get directions
	def directions(self):
		self.driver.implicitly_wait(10)
		directions = self.driver.find_element(By.XPATH, "//*[@id='pane']/div/div[1]/div/div/div[4]/div[1]/button/span") or self.driver.find_element(By.XPATH, '//*[@id="QA0Szd"]/div/div[1]/div[2]/div/div[1]/div/div/div[4]/div[1]/button/span')
		directions.click()

	# find place
	def find(self, adress):
		self.driver.implicitly_wait(10)
		find = self.driver.find_element(By.XPATH, '//*[@id="sb_ifc51"]/input')
		find.click()
		find.send_keys(adress)
		self.driver.implicitly_wait(10)
		serch = self.driver.find_element(By.XPATH, '//*[@id="directions-searchbox-0"]/button[1]')
		serch.click()
		walking = self.driver.find_element(By.CSS_SELECTOR, '[aria-label="Walking"]')
		walking.click()

	def time(self):
		self.driver.implicitly_wait(10)
		Totalminut = self.driver.find_element(By.XPATH, '//*[@id="section-directions-trip-0"]/div/div[3]/div[1]/div[1]').text
		times = list(Totalminut.split(' '))
		if times[1] == 'hr':
			time = int(times[0]) * 60 + int(times[2])
		else:
			time = int(times[0])
		return time

	def fill_array(self):
		for x in range(len(self.address) - 1):
			self.site()
			self.searchplace(self.address[x])
			self.directions()
			for i in range(x+1, len(self.address)):
				self.find(self.address[i])
				time = self.time()
				self.array_time[x][i] = time
				self.array_time[i][x] = time
		return self.array_time

	def sum_min(self):
		sum_time = []
		for i in self.array_time:
			sum_time.append(sum(i))
		minimum = sum_time.index(min(sum_time))
		meet = self.array_time[minimum]
		return meet

	def max_min(self):
		sum_max = []
		for i in self.array_time:
			sum_max.append(max(i))
		minimum = sum_max.index(min(sum_max))
		meet = self.array_time[minimum]
		return meet

	def main(self, meet):
		total = Fore.GREEN + str(sum(meet)) + Style.RESET_ALL
		name = Fore.RED + self.names[self.array_time.index(meet)] + Style.RESET_ALL
		min = sorted(meet)[1] # בחירת מינימום שאינו אפס
		fer = meet.index(max(meet))
		fer_name = Fore.RED + self.names[fer] + Style.RESET_ALL
		fer_time = Fore.GREEN + str(meet[fer]) + Style.RESET_ALL
		near = meet.index(min)
		near_name = Fore.RED + self.names[near] + Style.RESET_ALL
		near_time = Fore.GREEN + str(meet[near]) + Style.RESET_ALL
		print(f'meet at {name} \n{near_name} is the nearest. walking time: {near_time} minutes\n'
			  f'{fer_name} is the farthest. walking time:{fer_time} minutes\ntotal walking time:{total} minutes')



gm = Search()
gm.fill_array()

print('first option:')
gm.main(gm.sum_min())
print('second option:')
gm.main(gm.max_min())